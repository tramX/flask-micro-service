from flask import Flask
from flask_jsonrpc import JSONRPC
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

app = Flask(__name__)
CORS(app, supports_credentials=True)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
admin = Admin(app, name='FlaskMicroService', template_mode='bootstrap3')

jsonrpc = JSONRPC(app, '/api', enable_web_browsable_api=True)


class ApiRecord(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), unique=False, nullable=False)

    def __repr__(self):
        return '<ApiRecord %r>' % self.title


admin.add_view(ModelView(ApiRecord, db.session))


@jsonrpc.method('App.index')
def index(title):
    db.create_all()
    new_record = ApiRecord(title=title)
    db.session.add(new_record)
    db.session.commit()
    return 'Welcome to Flask JSON-RPC'


if __name__ == '__main__':
    app.run(debug=True)
